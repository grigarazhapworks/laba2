import java.util.Scanner;

public class Coffee {
    static Scanner scan = new Scanner(System.in);
    static int price;

    public static byte chooseGlass() {
        System.out.println("Размер стаканчика:\n1 - маленький\n2 - средний\n3 - большой\n4 - очень большой");
        byte glassSize = scan.nextByte();
        System.out.println("Тип кофе:");
        switch (glassSize) {
            case 1:
            case 2:
                System.out.println("1 - эспрессо\n2 - американо\n3 - латте\n4 - капучино\n5 - горячий шоколад");
                break;
            case 3:
            case 4:
                System.out.println("2 - американо\n3 - латте\n4 - капучино\n5 - горячий шоколад");
                break;
            default:
                System.out.println("Ошибка");
                glassSize = chooseGlass();
                break;
        }
        return glassSize;
    }

    public static byte chooseType(byte glassSize) {
        byte coffeeType = scan.nextByte();
        switch (coffeeType) {
            case 1:
                if (glassSize > 2) {
                    System.out.println("Ошибка");
                    System.exit(0);
                }
                price = (int) (100 + (100 * (glassSize / 10 - 0.1)));
            case 2:
                price = (int) (200 + (200 * (glassSize / 10 - 0.1)));
                break;
            case 3:
                price = (int) (220 + (220 * (glassSize / 10 - 0.1)));
                break;
            case 4:
                price = (int) (220 + (220 * (glassSize / 10 - 0.1)));
                break;
            case 5:
                price = (int) (230 + (230 * (glassSize / 10 - 0.1)));
                break;
            default:
                System.out.println("Ошибка");
                coffeeType = chooseType(glassSize);
                break;
        }
        return coffeeType;
    }

    public static byte isMilk() {

        System.out.println("Добавить молоко?\n1 - да\n 2 - нет");
        byte addMilk = scan.nextByte();
        switch (addMilk) {
            case 1:
                price += 50;
                break;
            case 2:
                break;
            default:
                System.out.println("Ошибка");
                addMilk = isMilk();
                break;

        }
        return addMilk;
    }

    public static byte sugarLevel() {
        System.out.println("Выберите уровень сахара:");
        byte sugar = scan.nextByte();
        switch (sugar) {
            case 0:
                break;
            case 1:
                price += 5;
                break;
            case 2:
                price += 10;
                break;
            case 3:
                price += 15;
                break;
            case 4:
                price += 20;
                break;
            case 5:
                price += 25;
                break;
            default:
                System.out.println("Ошибка");
                sugar = sugarLevel();
                break;
        }
        return sugar;
    }

    public static byte chooseTopping() {
        System.out.println("Топпинг\n0 - без топпинга\n1 - сливки\n2 - взбитые сливки\n3 - другой топпинг");
        byte topping = scan.nextByte();
        switch (topping) {
            case 0:
                break;
            case 1:
            case 2:
            case 3:
                price += 50;
                break;
            default:
                System.out.println("Ошибка");
                topping = chooseTopping();
        }
        return topping;
    }

    public static byte chooseSyrup() {
        System.out.println("Сироп\n0 - без сиропа\n1 - ванильный\n2 - карамельный\n3 - малиновый\n4 - мятный\n5 - кокосовый\n6 - миндальный");
        byte syrup = scan.nextByte();
        switch (syrup) {
            case 0:
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                price += 50;
                break;
            default:
                System.out.println("Ошибка");
                syrup = chooseSyrup();
        }
        return syrup;
    }

    public static byte chooseAlcohol() {
        System.out.println("Алкогольный напиток\n0 - без алкогольных напитков\n1 - пиво\n2 - вино\n3-шампанское\n4-мартини");
        byte alcohol = scan.nextByte();
        switch (alcohol) {
            case 0:
                break;
            case 1:
                price += 100;
                break;
            case 2:
                price += 175;
                break;
            case 3:
                price += 150;
                break;
            case 4:
                price += 125;
                break;
            default:
                System.out.println("ошибка");
                alcohol = chooseAlcohol();
        }
        return alcohol;
    }

    public static byte chooseDessert() {
        System.out.println("Десерт\n0 - без десерта\n1 - мороженое\n2 -морковный торт\n3 - круассан с яблоком\n4 - круассан с вишней");
        byte dessert = scan.nextByte();
        switch (dessert) {
            case 0:
                break;
            case 1:
                price += 75;
                break;
            case 2:
                price += 90;
                break;
            case 3:
            case 4:
                price += 50;
                break;
            default:
                System.out.println("ошибка");
                dessert = chooseDessert();
        }
        return dessert;
    }
    public static void coffeeCheck(byte glassType,byte coffeeType,byte addMilk,byte sugar,byte topping, byte syrup,byte alcohol,byte dessert) {
        System.out.print("Ваш заказ\nРазмер стаканчика: ");
        switch (glassType) {
            case 1:
                System.out.println("маленький");
                break;
            case 2:
                System.out.println("средний");
                break;
            case 3:
                System.out.println("большой");
                break;
            case 4:
                System.out.println("Очень большой");
                break;
        }
        System.out.print("Тип кофе: ");
        switch (coffeeType) {
            case 1:
                System.out.print("эспрессо " + (int) (100 + (100 * (glassType / 10 - 0.1))));
                break;
            case 2:
                System.out.print("американо " + (int) (200 + (200 * (glassType / 10 - 0.1))));
                break;
            case 3:
                System.out.print("латте " + (int) (220 + (220 * (glassType / 10 - 0.1))));
                break;
            case 4:
                System.out.print("капучино" + (int) (220 + (220 * (glassType / 10 - 0.1))));
                break;
            case 5:
                System.out.print("горячий шоколад" + (int) (230 + (230 * (glassType / 10 - 0.1))));
                break;
        }
        System.out.println(" руб");
        System.out.print("С молоком: ");
        switch (addMilk) {
            case 1:
                System.out.println("Да");
                break;
            case 2:
                System.out.println("Нет");
                break;
        }
        System.out.print("Уровень сахара: " + sugar + " " + sugar * 5);
        System.out.println(" руб");
        System.out.print("Топпинг: ");
        switch (topping) {
            case 0:
                System.out.print("Без топпинга");
                break;
            case 1:
                System.out.print("Сливки " + 50);
                break;
            case 2:
                System.out.print("Взбитые сливки " + 50);
                break;
            case 3:
                System.out.print("Другой " + 50);
                break;
        }
        if(topping!=0){
            System.out.println(" руб");
        }else{
            System.out.println();
        }
        System.out.print("Сироп: ");
        switch (syrup) {
            case 0:
                System.out.print("Без сиропа");
                break;
            case 1:
                System.out.print("ванильный " + 50);
                break;
            case 2:
                System.out.print("карамельный " + 50);
                break;
            case 3:
                System.out.print("малиновый " + 50);
                break;
            case 4:
                System.out.print("мятный " + 50);
                break;
            case 5:
                System.out.print("кокосовый" + 50);
                break;
            case 6:
                System.out.print("миндальный" + 50);
                break;
        }
        if(syrup!=0){
            System.out.println(" руб");
        }else{
            System.out.println();
        }
        System.out.print("Алкоголь: ");
        switch (alcohol) {
            case 0:
                System.out.print("без алкогольных напитков");
                break;
            case 1:
                System.out.print("Пиво " + 100);
                break;
            case 2:
                System.out.print("Вино " + 175);
                break;
            case 3:
                System.out.print("Шампанское " + 150);
                break;
            case 4:
                System.out.print("Мартини " + 125);
                break;
        }

        if(alcohol!=0){
            System.out.println(" руб");
        }else{
            System.out.println();
        }
        System.out.print("Десерт: ");
        switch (dessert) {
            case 0:
                System.out.print("без десерта");
                break;
            case 1:
                System.out.print("Мороженое "+75);
                break;
            case 2:
                System.out.print("Морковный торт "+90);
                break;
            case 3:
                System.out.print("Круассан с яблоком "+50);
                break;
            case 4:
                System.out.print("Круассан с вишней "+50);
                break;
        }
        if(dessert!=0){
            System.out.println(" руб");
        }else{
            System.out.println();
        }
    }
    public static void getResults() {
        price = 0;
        byte glassType=chooseGlass();
        byte coffeeType = chooseType(glassType);
        byte addMilk = isMilk();
        byte sugar = sugarLevel();
        byte topping=chooseTopping();
        byte syrup=chooseSyrup();
        byte alcohol=chooseAlcohol();
        byte dessert=chooseDessert();
        coffeeCheck(glassType,coffeeType,addMilk,sugar,topping,syrup,alcohol,dessert);
        System.out.println("Введите 1 для приступления к оплате заказа и 2 для изменения заказа");
        byte isready=0;
        while(isready!=1||isready!=2) {
            isready = scan.nextByte();
            switch (isready) {
                case 1:
                    System.out.println("Общая цена, не включая НДС: "+(price-(int)(0.2*(1/1.2)*price)));
                    System.out.println("НДС: " +(int)(0.2*(1/1.2)*price));
                    System.out.println("Общая цена, включая НДС: "+price);
                    int money=0;
                    int priceraz = price;
                    while(money<priceraz){
                        System.out.println("Внесите деньги. Осталось внести "+priceraz);
                        money = scan.nextInt();
                        priceraz -=money;
                        money = 0;
                    }
                    int tip=0;
                    if(priceraz<0){
                        priceraz=Math.abs(priceraz);
                        System.out.println("У вас осталась сдача "+priceraz+" руб. Выберите, сколько потратить на чаевые.");
                        tip = scan.nextInt();
                        while(tip>priceraz){
                            System.out.println("У вас нет столько денег.");
                            tip = scan.nextInt();
                        }
                        priceraz -= tip;
                    }
                    coffeeCheck(glassType,coffeeType,addMilk,sugar,topping,syrup,alcohol,dessert);
                    System.out.println("Общая цена, не включая НДС: "+(price-(int)(0.2*(1/1.2)*price)));
                    System.out.println("НДС: " +(int)(0.2*(1/1.2)*price));
                    System.out.println("Общая цена, включая НДС: "+price);
                    System.out.println("Сдача: "+priceraz);
                    System.out.println("Чаевые: "+tip);
                    break;
                case 2:
                    getResults();
                    break;
            }
        }


    }

    public static void main(String[] args) {
        getResults();
    }
}
